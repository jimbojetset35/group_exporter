#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import subprocess
import sys
import xlwt
import os.path


def options():
    parser = argparse.ArgumentParser(
        description="Tool for extracting groups and users from Windows Servers"
    )

    parser.add_argument('-x', '--export',
                        help='Export groups and users from windows servers',
                        action='store_true')
    parser.add_argument('-f', '--file',
                        help="input file. Must be on format env;server",
                        action='store')
    parser.add_argument('-u', '--user',
                        help="username",
                        action='store')
    parser.add_argument('-p', '--password',
                        help="password",
                        action='store')
    parser.add_argument('-d', '--domain',
                        help="Domain name.",
                        action='store')
    parser.add_argument('-o', '--output',
                        help="Output name file",
                        action='store')

    args = parser.parse_args()

    if args.export:
        result_list = []
        for data in read_file(args.file):
            print('Starting on server {}'.format(data['server']))
            a = list_users_from_groups(data['env'],
                                       data['server'],
                                       data['domain'],
                                       args.user,
                                       args.password)
            for i in a:
                result_list.append(i)
        write_xls_file(result_list, args.output)


def list_users():
    return subprocess.call('net users')


def list_localgroups(server, domain, user, password):
    group_list = []
    cmd = 'psexec \\\\{} -u {}\\{} -p {} net localgroup'.format(server,
                                                                domain,
                                                                user,
                                                                password)
    run = subprocess.Popen(cmd,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           shell=True)
    for line in run.stdout:
        line = line.strip()
        if "*" in line:
            group = line.replace('*', '')
            group_list.append(group)
    return group_list


def read_file(filename):
    f = open(filename, 'r')
    result = []
    for line in f:
        dictionary = {}
        line = line.strip()
        dictionary['env'] = line.split(';')[0]
        dictionary['server'] = line.split(';')[1]
        result.append(dictionary)
    return result


def write_xls_file(inputs, output_file):
    book = xlwt.Workbook(encoding="utf-8")
    sheet1 = book.add_sheet('Users by Group')
    sheet1.write(0, 0, "Environment")
    sheet1.write(0, 1, "Server")
    sheet1.write(0, 2, "Group")
    sheet1.write(0, 3, "User")
    line = 1
    for lines in inputs:
        sheet1.write(line, 0, lines.split(',')[0])
        sheet1.write(line, 1, lines.split(',')[1])
        sheet1.write(line, 2, lines.split(',')[2])
        sheet1.write(line, 3, lines.split(',')[3])
        line += 1
    book.save("{}.xls".format(output_file))


def list_users_from_groups(env, server, domain, user, password):
    groups = list_localgroups(server, domain, user, password)
    user_list = []
    for group in groups:
        print('  Listing users for group {}'.format(group))
        cmd = 'psexec \\\\{} -u {}\\{} -p {} ' \
              'net localgroup {}'.format(server, domain,
                                         user, password, group)
        run = subprocess.Popen(cmd,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=True)
        for line in run.stdout:
            dictionary = {}
            line = line.strip()
            if line.startswith('Alias name'):
                pass
            elif line.startswith('Comment'):
                pass
            elif line.startswith('Members'):
                pass
            elif line.startswith('-'):
                pass
            elif line.startswith('The command'):
                pass
            elif not line:
                pass
            else:
                dictionary['env'] = env
                dictionary['server'] = server
                dictionary['group'] = group
                dictionary['user'] = line
                result = '{},{},{},{}'.format(env, server, group, line)
                user_list.append(result)
                print('    Found User: {}'.format(line))
    return user_list

if __name__ == '__main__':
    try:
        url = 'https://technet.microsoft.com/en-us/sysinternals/psexec.aspx'
        try:
            os.path.isfile('psExec.exe')
        except IOError:
            sys.exit('psExec.exe not found, '
                     'please download it from: {}'.format(url))
        else:
            options()
    except KeyboardInterrupt:
        sys.exit("Shutdown requested...exiting")

