This guy uses psexec to connect on Windows servers to  gather the groups and users information and export it to an excel file.
### How do I get set up? ###

First of all, this script was developed using Python 2.7, but soon i will create a 3.5 version of it. Second, make sure you have the following modules installed:

* argparse
* xlwt

or just run:

```
#!python

pip install argparse  xlwt
```

You can also generate a single exe file by using pyInstaller ( compile it on a 32bits system to ensure compatibility)
```
#!python

pyinstaller --onefile group_extractor.py
```


### usage ###



```
#!python

  -h, --help                       show this help message and exit
  -x, --export                     Export groups and users from windows servers
  -f FILE, --file FILE             Input file. Must be on format env;server
  -u USER, --user USER             Admin username
  -p PASSWORD, --password PASSWORD Admin password
```